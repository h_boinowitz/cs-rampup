# CS RampUp

## Basic Structure

The repository consists of two folders
- `code` where the Python Scripts and other executables for the exercise sheets of the IfIS are stored
- `obsidian_vault` can be opened as a vault in [Obsidian](https://obsidian.md). 

> :information_source: **Info** \
The configuration files Obsidian creates in the `.obsidian`-directory are **not** pushed to GitLab (this folder is excluded by the respective [.gitignore](/obsidian_vault/.gitignore)-file). Make sure the `obsidian_vault` is opened as root-directory in Obsidian.
