## Procedure for Estimation
```mermaid
flowchart TD
Preparation-->Estimation
Estimation-->measuringComparisonLearning[measuring, comparison, learning]
measuringComparisonLearning-->re-estimation
re-estimation-->measuringComparisonLearning
```
**Best practices**:
- Good preparation is essential for the estimation.
- Completing and structuring the estimation basis (osintots – **o**h **s**hit, **I** **n**ever **t**hought **of** **t**his)
- Collect all factors
- Re-estimating and learning from project experience is a constant cycle throughout the project.

Ways to Estimate:
- [[Top-Down Estimation]]
- [[Bottom-Up Estimation]]
