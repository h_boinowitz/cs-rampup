## Bottom-Up Estimation
**Question**: If I want to do all of it, how much does it cost?

From the work packages to the budget:  
- Estimation of the individual work packages  
- The sum is the total budget.  

Estimation of ==total costs== as working ==basis for further planning==

### See also
- [[Top-Down Estimation]]