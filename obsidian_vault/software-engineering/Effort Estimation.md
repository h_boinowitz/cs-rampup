## Effort Estimation
**Scope of functionality is known**:
```mermaid
flowchart LR
FunctionalConcept[Functional Concept]-->TechnicalConcept[Technical Concept]
TechnicalConcept-->Realisation
Realisation-->Test&Integration
```
**Estimations are "unfair"**:
- They take place at a ==very early stage==.
- ==Little knowledge== about project.
- Nevertheless: You are bound by these estimations.


**If estimations are so flawed and difficult, why does one do them anyway?**:

> Even a "wrong" estimation is better than a complete blind flight.

- An estimation is the basis of [[planning]].
- While executing the plan, you will notice quite early, if an estimation is wrong and make adjustments.
- Estimating is also a [[process]]: as soon as initial experience and calculated costs have been determined in a project, re-estimation and correction follows.
- The estimation will become more accurate as the project progresses.

### See also
[[Consequences of Mis-Estimation]]
[[Procedure for Estimation]]