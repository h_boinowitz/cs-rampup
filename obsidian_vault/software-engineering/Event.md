## Event
An event signals the occurrence of a defined and describable state in the project  
flow (e.g. milestones).