## Network diagrams
Operations, [[Event|events]] and their dependencies are displayed in a  
network diagram.

The network diagram is a graphical representation of flow structures that  
illustrates the logical and temporal sequence of events.
#### Basic types of network diagrams
| |Arrow Diagram Method|Event Node–Network Diagram|Activity Node– etwork Diagram|
|-|------------------------|---------------------------------|----------------------------|
|Nodes|Events|Events|Activities|
|Arrow|Activities|Dependencies|Dependencies
|Presentation|![[Arrow_Diagram_method_example.png]]|![[Event_Node_Diagram_example.png]]|![[Activity_Node_Diagram_example.png]]|
