## Expert estimation
In an expert estimation, at least two experts are usually involved  in order to be able to compare estimation results

**Types of expert estimations**
- **Simple estimation**: Expert estimates the effort of the task based on task description and his/her experience
	- **Pro**: Simple approach, Fast, no calculation effort, no parameter estimation
	- **Cons**: (Extremely) subjective, High uncertainty factor, No transparency of the estimation process
- **Delphi-based estimations**:
	- **Standard-Delphi-Procedure**: Experts estimate completely independently from each others
	- **Broadband-Delphi-Procedure**: Experts discuss their interim results