## Work breakdown structure (WBS)
A work breakdown structure is a hierarchical decomposition of the total scope of work in a project into single tasks and work packages.