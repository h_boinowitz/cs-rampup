## Planning
- A plan shows the feasibility of a project.  If you can not even make a plan, then the project is not feasible.
- A plan is continuously tracked and updated in a project. This allows the project leader to finish a project.

A plan is the basis for controlling a project:  
- Without a plan, you will only be able to see at the end of the project whether the project is successful.  
- With control: hazards are recognizable early, so that one can react to it


**Basic concepts of planning**:
- A plan is created at the beginning of the project and then constantly refined and adapted.
- Planning is out of date once it's done (and sometimes even while it's being created).
- Planning is not a prediction. You can not compute a project.
- Planning is the project manager's most important working tool.

**Planning techniques**:
- [[Work breakdown structure]]
- [[Gantt-Chart]]
- [[Network diagrams]]