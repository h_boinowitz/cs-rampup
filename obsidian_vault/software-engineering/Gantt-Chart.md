## Gantt-Chart
**Basic Idea**:
- Project schedule represented by bars on a calendar.
- Advantage: Intuitive – you can immediately see the duration of the [[Process|processes]]
- Dependencies are less clearly displayed
- Developed by Henry L. Gantt (1861–1919)

**In comparison with other planning techniques**:
- Represents the project activities over time
- Most widespread in practice
- Support via tools, often default view