### Main Principles
- interaction
- working software
- responsiveness
- customer collaboration

### Pros and Cons
- **Pros**
	- can adapt to change
	- can deploy features fast, if they are urgent
	- customer gives feedback immediately
- **Cons**
	- pressure on devs $\to$ sprints might lead to an environment, where code is not tested properly anymore because of the ==limited timeframe== assigned to tasks
	- introduces overhead (meetings, product owners)

### Implementations
- [[SCRUM]]