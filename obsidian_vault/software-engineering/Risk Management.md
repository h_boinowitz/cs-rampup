### Risk
The probability of the occurrence of a hazard that causes damage combined with the severity of a  damage.
$$R = ED \cdot {FD}$$
$\to$ *Risk = Extent of Damage $\cdot$ Probability of Event*

*Potential Risk factors*:
- **customer** (lack of understanding, budget)
- **developers** (qualification, staff shortages, ...)
- **task** (requirements)
- **company** (recruiting, working conditions, ...)
- **environment** (politics, supply chain, ...)


### Risk Management
Systematic application of methods to identify, analyze, prioritize and control risks.

*Tasks*:
- Determination of the risk context
- Risk identification 
- Risk analysis and prioritization 
- Risk control and mitigation
- Risk review and monitoring

*Purpose*:
- confidence
- more accurate [[Effort Estimation]]
- increases chances of success 🏆
- less surprises 😬

### Dealing with risks
- prevention $\to$ try to identify and avoid risks by [[Risk Management#Risk Management|Risk Management]]
- have a ==Plan B== in case of emergency 🔥