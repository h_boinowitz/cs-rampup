Split **static** and **dynamic** model

### Static Model
- classes
- interfaces
- relations

### Dynamic Model
- transitions
- states

### See also
- [[Unified Modelling Language (UML)]]