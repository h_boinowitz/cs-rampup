## Top-Down-Estimation
**Question**: If I have a ==fixed budget==, how much can the individual work packages cost?

From the fixed effort to the work packages:
- Design the project in a manner, that it stays within the budget
- Tasks are done just as "well" as there is budget

In contrary one can also use a [[Bottom-Up Estimation]]