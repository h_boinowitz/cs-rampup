- general purpose modelling language in the field of [[Software Engineering]] 
- standardised way to visualise the design of a system

### Diagrams
- the diagrams can be split into diagrams concerning the **dynamic** model and those concerning the **static model** (see [[Object-oriented Analysis (OOA)]])
- diagrams introduced in the lecture

| Diagram                    | Purpose                                                                                                                         |
| -------------------------- | ------------------------------------------------------------------------------------------------------------------------------- |
| Use Case Diagram           | Visualise Actors, Relationships between Use Cases                                                                               |
| Package Diagram            | Visualise how the software is partitioned, find sensible units                                                                  |
| Class Diagram (macrolevel) | Visualise how the classes are connected with each other (relations, inheritance) $\to$ close to [[ER Diagram]]                  |
| Class Diagram (microlevel) | More detailed layout of the classes/interfaces, plan attributes and functions for each class                                    |
| Activity Diagram           | Description of behavior, especially processes or workflows                                                                      |
| State Chart                | Visualise transitions between different software states and the conditions, that have to be met for these transitions to happen |
| Sequence Diagram           | Shows the message or event flow between objects                                                                                                                                |
| Component Diagram          | Show, how the interfaces are dependent on each other and which interfaces belong to the same part of the software               |
| Deployment Diagram         | Show how hard- and software components interact with each other                                                                 |



