## Consequences of Mis-Estimation
**too low**: 
- Budget is not enough, project can not be done in the budget

**too high** 💰: 
- Estimated value is higher than the actual costs.
- ==Hard to detect in retrospect==. Each project takes full advantage of the budget.

**Consequence**: Estimates likely to be too high.
**Danger**: Business case does not pay off, or project is lost to competitors.

