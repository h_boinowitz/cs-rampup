## Named Entity Recognition
- takes **texts** and outputs the detected **named entities**
- usually outputs the entity **mention** and its **type**
- works with **rules** (either hand-written or learned by deep learning architectures)

- utilizes
	- **Global features** include all known information about a word/named [[entity]] (e.g. databases, dictionaries, latent models, word distributions)
	- **Local features** include the local context (usually the sentence) in which a named entity is mentioned!


![[NER_example.png]]