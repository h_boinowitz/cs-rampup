[[Entity]] that does not exist without the other "strong" entity it belongs to. Hence, the existence of a weak entity is tightly coupled with the existence of the "strong" entity

### See also
- [[Total Participation]]