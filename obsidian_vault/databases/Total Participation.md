- all entities of an entity type appear in a certain relationship set
- e.g. Each driver's license belongs to exactly one person

### See also
- [[Weak Entity]]