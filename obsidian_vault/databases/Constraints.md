Additional properties the tuples we insert into our [[Database]] have to fulfill.
Some of those **integrity constraints** can not be modelled in ER.

But, there are constraints, that can be modelled:
- [[Foreign Key Constraint]]
- [[Primary Key Constraint]]
- `NOT NULL`-constraint

### See also
- [[Data Model]]