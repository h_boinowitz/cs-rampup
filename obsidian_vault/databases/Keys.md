- Single Key attribute
- Composite key $\to$ multiple key attributes
- primary key is not allowed to be `NULL` 

>[!WARNING]
>In a composite key each attribute itself **does not** need to be unique
