- clarifying the **role** of an entity in a relationship
```mermaid
graph TD;
Person--supervisor-->supervises
supervises--supervisee-->Person
```
