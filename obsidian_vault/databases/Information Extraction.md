## Information Extraction
Information extraction is a [[NLP tasks|NLP task]] that extracts structured from unstructured information

Extract information from, e.g. 
- texts
- tables

```mermaid
graph LR;
id1["<b>Text</b> <br/> (<i>unstructured</i> data)"] --> id2[<b>Structured Data</b>]
```
