- find corresponding rows in two different tables
- add information from one table to another
- important, when large table is split into multiple small tables (e.g due to [[Normalization]])

### $\theta$-Joins
- $\Join_{\,\theta}$ (SQL: `JOIN {table 2} ON {condition}`)
- $\theta$ is boolean condition
- equivalent to $\sigma_\text{condition} (R \times S)$