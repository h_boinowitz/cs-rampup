## Semantic Web
- W3C’s vision💡 of the Web of linked data
- enable people to 
	- create data stores on the Web, 
	- build vocabularies
	- write rules for handling data. 
- is empowered by technologies such as [[RDF]], SPARQL, OWL, and SKOS