## Data-Driven Systems

### Data 
- different types of data
	- structured
	- semi-structured
	- unstructured
- data lakes vs. data swamps
- challenges in modern data-driven systems
	- data cleaning
	- data management

### Databases
- data models
	- structure (data structures to represent the data)
	- constraints (rules the data has to fulfill)
	- manipulation (querying)
- building blocks
	- entities
		- weak entities
	- attributes
	- relations (show how the entity sets interact with each other)
- database models
	- conceptual model
	- logical model
	- physical model
	$\to$ schema
- ER diagrams (represent relations between entities)
	- components (entities, relations, attributes)
	- translation into logical models

### Relational data bases 
- relation $\to$ mathematical definition
- constraints
	- key constraints (can be directly represented in ER)
	- uniqueness constraint
	- `NOT NULL`-constraint
	- foreign key constraint
- relational algebra (RA)
	- strategy for data manipulation
	- set operations ($\cup$, $\cap$, $\setminus$ , $\times$ )
	- RA operations
		- selection $\sigma$ (SQL: `WHERE`, pyspark: `filter`)
		- projection $\pi$ (SQL: `SELECT DISTINCT {cols}`, pyspark: `select`)
		- renaming $\rho$ (SQL: `AS`, pyspark: `withColumnRenamed`)
- Extended RA
	- extends RA with **joins**
	- joins
		- $\Join_{\,\theta}$ (SQL: `JOIN {table 2} ON {condition}`)
		- $\theta$ is boolean condition
		- equivalent to $\sigma_\text{condition} (R \times S)$
- table design
	- redundancy
	- anomalies
- [[Normalization]]
	- [[Normalform]]


### Knowledge Graphs and Semantic Web
- [[Semantic Web]]
- [[URI]]
- [[RDF]]
	- [[RDF Turtle]]
	- [[RDF Reification]]
	- [[RDF Schema]]
- [[OWL]]
- [[Knowledge Graph]]
	- [[DBPedia]]
	- [[Wikidata]]
- [[SPARQL]]

### Text Mining
- [[Text Mining]]
- [[NLP]]
	- [[Syntactical analysis]]
	- [[Semantical analysis]]
	- [[Statistical language model]]
	- [[NLP tasks]]
	- [[Entity Detection]]
	- [[Named Entity Recognition]]
	- [[Entity Linking]]
	- [[Information Extraction]]
	- [[Relation Extraction]]
	- [[Open Information Extraction]]


## Software Engineering

### Quality citeria
- quality of use (How good is it **in practice**?)
- outer and inner quality (How good is it **in theory**?)

### Process Models
- benefits of usage
- commonly used process models in software development
	- waterfall
	- V-model
	- SCRUM
- agile software development

### Requirements Analysis
- types of requirements
	- non-functional/technical requirements (**How** shall it be implemented?)
	- functional requirements (**What** shall it do?)
- criteria for good requirements
- modelling requirements
	- tools
	- use cases
- use case diagrams
	- actors
	- relationships between actors and use cases (multipliers, associations)
	- generalizations
	- relationships between use cases (`<<include>>` , `<<extend>>`)

### Object-oriented Analysis
- split **static model** (classes, relations) and **dynamic model** (states, transitions between states)

### Class diagrams
- macro level
	- building blocks
		- classes
		- associations
		- aggregations
		- compositions
		- inheritance
- micro-level
	- building blocks
		- attributes
		- operations
		- data types
		- default values
- further concepts
	- interfaces
	- comparison: abstract class $\leftrightarrow$ interface
		[Abstract Base Classes in Python 🐍](https://docs.python.org/3/library/abc.html)

### Package diagrams
- structure of the modelled system
- package
	- understandable on its own
	- strong cohesion
	- weak coupling with other packages

## Testing
- Testing in Python 🐍: [Pytest Documentation](https://docs.pytest.org/en/latest/contents.html)
- More modern Functional Testing Framework: [Hypothesis](https://hypothesis.readthedocs.io/en/latest/)

- [[Quality]]
- [[Error Chain]]
- [[Testing]]
- [[Unit Test]]
- [[Integration Test]]
- [[System Test]]
- [[Acceptance Test]]
- [[Static Testing]]
- [[Dynamic Testing]]
	- [[Black-Box Testing]]
		- [[Equivalence classes testing]]
		- [[Boundary Value Analysis]]
	- [[White-Box Testing]]

### Project Management
- [[Planning]]
	- [[Work breakdown structure]]
	- [[Gantt-Chart]]
	- [[Network diagrams]]
- [[Effort Estimation]]
	- [[Consequences of Mis-Estimation]]
	- [[Procedure for Estimation]]
		- [[Top-Down Estimation]]
		- [[Bottom-Up Estimation]]
	- [[Estimation methods]]
		- [[Expert estimation]]
- [[Risk Management]]


## Security

### Basics
- Security goals
	- confidentiality
		Protection against access 
	- integrity
		Protection against manipulation
	- availability
		Protection against disruption
- threats
	- disclosure
	- deception
	- disruption
	- usurpation
- safety $\leftrightarrow$ security
- attack
- strategies
	- prevention (**avoid** issues)
	- detection (**find** issues)
	- analysis (**prevent** the same error to **reoccur**)
- security policy $\leftrightarrow$ security mechanism

### Cryptography
- cryptosystem (basic structure, encryption/decryption)
- cipher 
- key space
- symmetric cryptography $\leftrightarrow$ public-key cryptography
	- different sources of security
- properties of a strong cipher
	- confusion $\to$  hard to extract $K$ from $M$ and $C$
	- diffusion $\to$ hard to extract $M$ from $C$
	- Kerckhoff's principle
- symmetric ciphers
	- block ciphers
	- stream ciphers
- hash functions
	- one-way property
	- keyless and keyed hash functions
	- applications:
		- checksums
		- public-key cryptography
- key exchange (problems for multi-party cryptosystems)
- public and private keys (scalability, structure of a cryptosystem that uses this approach)
- Trapdoor one-way functions
	- usable problems
		- discrete logarithms
		- integer factorization
- Man-in-the-Middle Attacks
	- weaknesses of asymmetric cryptosystems
	- prevention mechanisms (key fingerprints, signing)

### Authentication
- confirm one's identity
	- knowledge (e.g password)
	- ownership (e.g credit card)
	- biometrics
$\to$ advantages and disadvantages
- multi-factor authentication
- passwords 
	- issues, attacks against passwords
	- security measures
		- hashing/salting
		- key stretching

### Access Control
- access control matrix
- access control list (ACL)
- capabilities
- access control models
	- objects/subjects
	- permissions
	- management of permissions

### Network attacks
- causes
	- vulnerable implementations
	- wrong configuration
	- ...
- defenses
	- encryption and verification of data (**cryptography**)
	- authentication of the peoples/machines using the network (**authentication**)
	- firewalls, blocking traffic from the outside (**access control**)
- reactive security concepts
	- vulnerability assessment
	- intrusion detection $\to$ attack
	- forensics $\to$ attackers
- firewalls
- network setups with DMZ

### Vulnerabilities
- origins (system design, misconfiguration, ...)
- can be prevented in the development process $\to$ needs time and expertise
- exploits
- vulnerability lifecycle
- types of vulnerabilities 
	- overflows (memory)
	- locks (concurrency)
	- code injections (validation)
- defenses
	- validation (especially of user input)
	- non-executable memory

### Attacks against ML
- models do not really "understand" anything, but make predictions based on a prediction function $\to$ ML is attackable, as the prediction function itself is a vulnerability
- possible timeframes for attack
	- during the learning process
	- in the application phase
- types of attacks
	- evasion 🌪 $\to$ attack against **integrity of the prediction**, 
		- adversarial ML
		- add noise to input to get wrong prediction result
	- inference $\to$ attack against **confidentiality 🛡 of the model** 
		- stealing the model, reconstructing the model from in- and outputs
		- find out, if certain samples were in the training data
	- poisoning ☠️ $\to$ attack against **integrity of the model**
		- manipulation of the training data $\to$ wrong predictions, backdoor attacks to manipulate the decisions of the models
- defenses
	- use ML-algorithms that are not (so) vulnerable
	- ⚠️ be aware that ML is attackable
	- current research
		- randomization (prediction function, features, ...)
		- complexity
		- certified robustness
		- stateful application (using accounts, authentication)
		- security-aware testing ($\to$ testing edge cases)
	$\to$ relatively ineffective, all **defence strategies have their limitations**

## Distributed Systems

### Basics
- definition of **Distributed Systems**
- middleware
- inherent distribution $\leftrightarrow$ distribution as an artefact
- reasons for "distribution as an artefact"
	- availability
	- fault tolerance
	- load balancing
- service interfaces
- centralized system $\leftrightarrow$ decentralized system

### DNS
- purpose
- iterative mode $\leftrightarrow$ recursive mode

### Characteristics of Distributed Systems
- points of failure
	- no global clock
	- partial failure
	- message delay
	- processing latency
- **heterogenity** of hardware. operating systems , ...
- openness
- large attack surface
- **scalability** in different dimensions
- **failure handling** (distributed system might recover due to redundancy)
- **transparency** (the user does not notice he/she is working on a complex distributed system)

### Inter-process Communication (IPC)
- two approaches
	- shared memory
	- message passing
- message passing interfaces
- message formats
- reliable $\leftrightarrow$ unreliable
- ordered $\leftrightarrow$ unordered
- usage of sockets (motivation)
- UDP/TCP
	- differences between datagrams and stream-based protocols
- head-of-line blocking
- modifications of TCP
	- Quic
	- HTTP
	- REST
	- RPC
- forwarding/synchronization to make sending to multiple receivers reliable
- strategies for many-to-many communication
	- usage of a sequencer (does not scale)
	- gossiping
	- publish/subscribe

### Fault tolerance
- terminology
	- failure
	- error
	- fault
- causes of **faults** 
	- physical (hardware wear-out)
	- software errors
	- interaction/configuration
- permanent faults $\leftrightarrow$ transient/intermitted faults
- types of **failures**
	- lates/early response
	- crash
	- incorrect/missing response
- consistent failure $\leftrightarrow$ inconsistent failure

### Dependability
- trustworthiness of a system
- aspects of a trustworthy system
	- reliability (no interruptions)
	- availability (high probability it works *correctly*, if you request the service)
	- safety (correct results)
	- security (robust against attacks)
- approaches
	- fault avoidance (do not let problems occur)
	- fault tolerance (if an error occurs, the service still works)
	- failure recovery (if an error occurs, the system come back quickly)
- different system levels, where failures can happen
	- networking failures
	- software failures
	- hardware/node failures (hardware)

### Hardware/Node failures
- replication
- different setups
	- primary/backup
	- state machine replication

### Scaling
- multicast
	- gossip
	- publish/subscribe
- use cases for data
	- request/reply
	- analytics
- approaches to scale data
	- sharding (partitioning)
	- MapReduce
- partitioning
	- avoid multi-partition requests
	- strategies to locate the data (3 different layouts)
- MapReduce
	- master orchestrates workers
	- workers do their calculations independently, then the results are combined in the reduce-step
	- limited programming model
	- high parallelism
	- simplified orchestration
